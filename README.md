# Gin Moderation Sidebar

A simple module that adds CSS to help the
[Moderation Sidebar](https://www.drupal.org/project/moderation_sidebar) toolbar
tab work well with [Gin Admin Theme](https://www.drupal.org/project/gin) /
[Gin Toolbar](https://www.drupal.org/project/gin_toolbar).

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/gin_moderation_sidebar)

- Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/gin_moderation_sidebar)


## Requirements

- Drupal core 9.3 or higher
- [Moderation Sidebar](https://www.drupal.org/project/moderation_sidebar)
- [Gin Admin Theme](https://www.drupal.org/project/gin)
- [Gin Toolbar](https://www.drupal.org/project/gin_toolbar)


## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Vist Administration » Configuration » User Interface » Gin Moderation Sidebar:

- Moderation Sidebar Tab Style
  Choose between 'Default' and 'High contrast' tab style options. The 'High
  Contrast' style will be used automatically if Gin's high contrast mode is
  enabled.


## Maintainers

Current maintainers:
  - Michael Caldwell - [justcaldwell](https://www.drupal.org/u/justcaldwell)
